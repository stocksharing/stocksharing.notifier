using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Stocksharing.Common.Interfaces;
using Stocksharing.Notifier.Data.DbContexts;
using Stocksharing.Notifier.Services.Interfaces;
using Stocksharing.Notifier.Services.Senders;
using Stocksharing.Notifier.Services.Services;
using System.Net.Mail;

namespace Stocksharing.Notifier
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // ����������� ��������� ������
            services.AddDbContext<NotifierDbContext>(options =>
                options.UseSqlServer(Configuration["ConnectionStrings:Notifier"], x => x.MigrationsAssembly("Stocksharing.Notifier.Data"))
            );

            // ����������� ��������
            if (Environment.IsDevelopment())
            {
                services.AddScoped<IMessageSender<MailMessage>, TestEmailSender>();
            }
            else
            {
                services.AddScoped<IMessageSender<MailMessage>, EmailSender>();
            }

            services.AddScoped<INotificationService<IEmail>, EmailService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
