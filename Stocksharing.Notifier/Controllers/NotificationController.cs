﻿using Microsoft.AspNetCore.Mvc;
using Stocksharing.Common.Interfaces;
using Stocksharing.Notifier.Models;
using Stocksharing.Notifier.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Stocksharing.Notifier.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class NotificationController : ControllerBase
    {
        [HttpPost("email")]
        public async Task<IActionResult> Email([FromBody] EmailRequest request, [FromServices] INotificationService<IEmail> service)
        {
            try
            {
                await service.NotifyAsync(request);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }
    }
}