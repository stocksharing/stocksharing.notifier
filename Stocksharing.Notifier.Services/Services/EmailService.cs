﻿using Stocksharing.Common.Interfaces;
using Stocksharing.Notifier.Data.DbContexts;
using Stocksharing.Notifier.Data.Entities;
using Stocksharing.Notifier.Data.Enums;
using Stocksharing.Notifier.Services.Interfaces;
using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Stocksharing.Notifier.Services.Services
{
    public class EmailService : INotificationService<IEmail>
    {
        private readonly NotifierDbContext _notifierDbContext;

        private readonly IMessageSender<MailMessage> _messageSenderService;

        public EmailService(NotifierDbContext notifierDbContext, IMessageSender<MailMessage> messageSenderService)
        {
            _notifierDbContext = notifierDbContext;
            _messageSenderService = messageSenderService;
        }

        public async Task NotifyAsync(IEmail message)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress(message.From),
                Subject = message.Subject,
                Body = message.Body
            };

            foreach (var recipient in message.To)
            {
                mailMessage.To.Add(new MailAddress(recipient));
            }

            var emailLog = new EmailLog
            {
                To = string.Join(";", message.To),
                From = message.From,
                Subject = message.Subject,
                Body = message.Body,
                CreatedDate = DateTime.Now,
                Status = MessageStatus.Success
            };

            try
            {
                await _messageSenderService.SendAsync(mailMessage);
            }
            catch (Exception exception)
            {
                emailLog.Status = MessageStatus.Fail;
                emailLog.ErrorMessage = exception.Message;
            }
            finally
            {
                await _notifierDbContext.EmailLogs.AddAsync(emailLog);
                await _notifierDbContext.SaveChangesAsync();
            }
        }
    }
}
