﻿using System.Threading.Tasks;

namespace Stocksharing.Notifier.Services.Interfaces
{
    /// <summary>
    /// Отправщик сообщений
    /// </summary>
    /// <typeparam name="T">Тип сообщения</typeparam>
    public interface IMessageSender<in T>
    {
        /// <summary>
        /// Отправить сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public Task SendAsync(T message);
    }
}
