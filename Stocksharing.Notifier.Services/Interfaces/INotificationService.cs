﻿using Stocksharing.Common.Interfaces;
using System.Threading.Tasks;

namespace Stocksharing.Notifier.Services.Interfaces
{
    /// <summary>
    /// Сервис для отправки уведомлений
    /// </summary>
    /// <typeparam name="T">Тип сообщения</typeparam>
    public interface INotificationService<in T> where T : IMessage
    {
        /// <summary>
        /// Уведомить
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public Task NotifyAsync(T message);
    }
}
