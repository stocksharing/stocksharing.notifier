﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Stocksharing.Notifier.Services.Senders
{
    public class TestEmailSender : EmailSender
    {
        private readonly string[] recipients;

        public TestEmailSender(IConfiguration configuration) : base(configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            recipients = configuration["TestRecipients"].Split(";");
        }

        public override async Task SendAsync(MailMessage message)
        {
            message.To.Clear();

            foreach (var recipient in recipients)
            {
                message.To.Add(new MailAddress(recipient));
            }

            await base.SendAsync(message);
        }
    }
}
