﻿using Microsoft.Extensions.Configuration;
using Stocksharing.Notifier.Services.Interfaces;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Stocksharing.Notifier.Services.Senders
{
    public class EmailSender : IMessageSender<MailMessage>
    {
        private readonly string _host;
        private readonly int _port;
        private readonly string _userName;
        private readonly string _password;
        private readonly bool _enableSsl;

        public EmailSender(IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            var section = configuration.GetSection("Smtp");

            _host = section["Host"];
            _port = int.Parse(section["Port"]);
            _userName = section["UserName"];
            _password = section["Password"];
            _enableSsl = bool.Parse(section["EnableSsl"]);
        }

        public virtual async Task SendAsync(MailMessage message)
        {
            using var client = new SmtpClient(_host, _port)
            {
                Credentials = new NetworkCredential(_userName, _password),
                EnableSsl = _enableSsl
            };

            await client.SendMailAsync(message);
        }
    }
}
