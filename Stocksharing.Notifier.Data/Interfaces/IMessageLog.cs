﻿using Stocksharing.Notifier.Data.Enums;
using System;

namespace Stocksharing.Notifier.Data.Interfaces
{
    /// <summary>
    /// Лог сообщений
    /// </summary>
    public interface IMessageLog
    {
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Статус отправки сообщения
        /// </summary>
        public MessageStatus Status { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
