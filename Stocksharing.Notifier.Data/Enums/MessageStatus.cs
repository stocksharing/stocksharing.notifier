﻿using System;

namespace Stocksharing.Notifier.Data.Enums
{
    /// <summary>
    /// Статус отправки сообщений
    /// </summary>
    public enum MessageStatus
    {
        /// <summary>
        /// Отправка не удалась
        /// </summary>
        Fail = 0,

        /// <summary>
        /// Отправка удалась
        /// </summary>
        Success = 1
    }
}
