﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Stocksharing.Notifier.Data.Entities;

namespace Stocksharing.Notifier.Data.EntityConfigurations
{
    public class EmailLogConfiguration : IEntityTypeConfiguration<EmailLog>
    {
        public void Configure(EntityTypeBuilder<EmailLog> builder)
        {
            builder.Property(p => p.To)
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(p => p.From)
                .HasMaxLength(340)
                .IsRequired();

            builder.Property(p => p.Subject)
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(p => p.Body)
                .IsRequired();

            builder.Property(p => p.ErrorMessage)
                .HasMaxLength(1000);

            builder.HasIndex(p => new { p.CreatedDate, p.Status });
        }
    }
}
