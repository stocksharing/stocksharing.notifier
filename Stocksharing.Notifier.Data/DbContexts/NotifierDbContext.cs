﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Stocksharing.Notifier.Data.Entities;
using System;
using System.Reflection;

namespace Stocksharing.Notifier.Data.DbContexts
{
    public class NotifierDbContext : DbContext
    {
        public DbSet<EmailLog> EmailLogs { get; set; }

        public NotifierDbContext()
        {
        }

        public NotifierDbContext(DbContextOptions<NotifierDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var basePath = AppContext.BaseDirectory;

            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Development.json");

            var configuration = builder.Build();

            optionsBuilder.UseSqlServer(configuration["ConnectionStrings:Notifier"]);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
