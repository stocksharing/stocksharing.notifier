﻿using Stocksharing.Notifier.Data.Enums;
using Stocksharing.Notifier.Data.Interfaces;
using System;

namespace Stocksharing.Notifier.Data.Entities
{
    /// <summary>
    /// Лог Email-сообщений 
    /// </summary>
    public class EmailLog : IMessageLog
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Получатели
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Отправитель
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Тема сообщения
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Тело сообщения
        /// </summary>
        public string Body { get; set; }

        public DateTime CreatedDate { get; set; }

        public MessageStatus Status { get; set; }

        public string ErrorMessage { get; set; }
    }
}
